import Vue from 'vue'
import Router from 'vue-router'
import PageNotFoundView from 'src/views/PageNotFoundView'
import HomeView from 'src/views/HomeView'
import App from 'src/App'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [{
    path: '/',
    name: 'home',
    component: HomeView
  }, {
    path: '*',
    name: 'pageNotFound',
    component: PageNotFoundView
  }]
})

// create the app instance.
new Vue({
  router,
  render: h => h(App)
}).$mount('app')
