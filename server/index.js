import http from 'http'
import createServer from './lib/createServer'
import config from 'config'
import logger from './lib/logger'

const log = logger('app')

createServer().then(app => {
  let server = http.Server(app.callback())
  server.listen(config.app.port)
  log.debug(`server ${config.app.name} started on ${config.app.port} in ${config.app.env} mode.`)
})
