import log4js from 'koa-log4'
import config from 'config'

log4js.configure(config.log4jsConfig)

const logger = (name) => {
  return log4js.getLogger(name)
}

export { logger as default, log4js }
