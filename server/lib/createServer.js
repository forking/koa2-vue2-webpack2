import Koa from 'koa'
import config from 'config'
import route from '../routes'
import logger from './logger'
import middlewares from '../middlewares'

const log = logger('app')

/**
 * Creates and returns a new Koa application.
 * Does *NOT* call `listen`!
 *
 * @return {Promise<Koa>} The configured app.
 */
export default async function createServer () {
  log.debug('Creating server...')
  const app = new Koa()
  app.keys = config.app.appKeys

  // Bootstrap middlewares
  // Must be used before any router is used
  // Best to .use() at the top before any other middleware, to wrap all subsequent middleware.
  app.use(middlewares())

  // Bootstrap routes
  app.use(route.allowedMethods())
  app.use(route.routes())

  // Default handler when nothing stopped the chain.
  // app.use(notFoundHandler)

  log.debug('Server created, ready to listen')
  return app
}
