import path from 'path'
import fsp from 'fs-promise'
import { env, root, ROOT, isProd, isDev } from './helpers'
import log4jsConfig from './log4js'

const clientDir = root('client')
const serverDir = path.resolve(__dirname, '..')
const modelsDir = path.resolve(serverDir, 'models')
const SECRET = 'DXXMYSJTYPXQQJSFQJGHLLCZDLSCHSCJRRWHXWYZJSWYDLBRDGLYWGXMMGSNSTNLZJHSKYLXJRZLYWWJHTJSGZZZWYLGCZNNC'
const jwtSecretKey = 'ZYWLFXXWJCCAASTJCSYGCZLWYSKPLYNBSKTYZDJDGQYHWWYDJKZBHWYXDRQC'
const jwtIssuer = 'baioh.com'
let jwtOptions = {
  secret: jwtSecretKey,
  issuer: jwtIssuer
}
let jwtPassthroughOptions = jwtOptions
jwtPassthroughOptions.passthrough = true

const configs = {
  app: {
    name: 'Baioh',
    env: env,
    port: process.env.PORT || 3000,
    secretKeys: SECRET,
    appKeys: ['YmFpb2g=', SECRET],
    isProd: isProd,
    isDev: isDev
  },
  dir: {
    root: ROOT,
    serverDir: serverDir,
    clientDir: clientDir,
    views: root('server/views'),
    log: root('logs'),
    public: root('public')
  },
  path: {
    assetsFile: root('public/static/assets.json')
  },
  log4jsConfig: log4jsConfig,
  require: {
    root: name => require(path.join(ROOT, name)),
    server: name => require(path.join(serverDir, name)),
    client: name => require(path.join(clientDir, name)),
    models: name => require(path.join(modelsDir, name))
  },
  join: {
    root: name => path.join(ROOT, name),
    server: name => path.join(serverDir, name),
    client: name => path.join(clientDir, name)
  }
}

// ensure directories in configs.dir
for (let d in configs.dir) {
  fsp.ensureDirSync(configs.dir[d])
}

// use module.exports for 'config'
module.exports = configs
