import path from 'path'
import fsp from 'fs-promise'
import { root, isDev } from './helpers'

const logRoot = 'logs'

const log4jsConfig = {
  appenders: [{
    type: 'console'
  }, {
    type: 'clustered',
    appenders: [{
      type: 'dateFile',
      filename: path.join(logRoot, 'access/access.log'),
      pattern: '-yyyy-MM.log',
      alwaysIncludePattern: true,
      category: 'http'
    }, {
      type: 'dateFile',
      filename: path.join(logRoot, 'app/app.log'),
      pattern: '-yyyy-MM.log',
      alwaysIncludePattern: true,
      category: 'app'
    }, {
      type: 'logLevelFilter',
      level: isDev ? 'DEBUG' : 'ERROR',
      appender: {
        type: 'dateFile',
        filename: path.join(logRoot, 'errors/errors.log'),
        pattern: '-yyyy-MM.log',
        alwaysIncludePattern: true,
        category: 'http'
      }
    }]
  }]
}

function appendersTraversing (obj, dirs) {
  if (obj.appenders) {
    obj.appenders.forEach(item => {
      appendersTraversing(item, dirs)
    })
  } else if (obj.appender) {
    appendersTraversing(obj.appender, dirs)
  } else if (obj.filename) {
    dirs.push(root(path.dirname(obj.filename)))
  }
}

function ensureLogDirs () {
  let dirs = []
  appendersTraversing(log4jsConfig, dirs)
  dirs.forEach(dir => fsp.ensureDirSync(dir))
}

// create sub log directory before use logger
ensureLogDirs()

export default log4jsConfig
