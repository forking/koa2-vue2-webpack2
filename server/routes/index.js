import Router from 'koa-router'
import config from 'config'

const router = Router()

router.get('/', async function index (ctx) {
  await ctx.render('index', {env: config.app.env})
})

export default router

