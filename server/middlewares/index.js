import compose from 'koa-compose'
import bodyParser from 'koa-bodyparser'
import compress from 'koa-compress'
import views from 'koa-views'
import historyApiFallback from 'koa-connect-history-api-fallback'
import responseTime from 'koa-response-time'
import helmet from 'koa-helmet'
import favicon from 'koa-favicon'
import koaStatic from 'koa-static'
import config from 'config'
import { log4js } from '../lib/logger'
import conditional from 'koa-conditional-get'
import etag from 'koa-etag'
import koaWebpackAssets from 'koa-webpack-assets'

/**
 * HMR
 */
import webpack from 'webpack'
import koaWebpack from 'koa-webpack'
const devConfig = require(config.join.root('build/webpack.dev.conf'))

let middlewares = [
  log4js.koaLogger(log4js.getLogger('http'), {
    level: 'auto'
  })
]
let historyOptions = {
  index: '/'
}
if (config.app.isDev) {
  // middlewares for development
  historyOptions.verbose = true
  middlewares.unshift(responseTime())

  /**
   * HMR
   */
  const compiler = webpack(devConfig)
  let devOpts = {
    publicPath: devConfig.output.publicPath,
    stats: {
      colors: true,
      chunks: false
    }
  }

  middlewares.push(koaWebpack({
    compiler: compiler,
    dev: devOpts
  }))
} else {
  // middlewares for production
  middlewares = middlewares.concat([
    compress(),
    helmet(),
    koaStatic(config.dir.public, {
      maxage: 365 * 24 * 60 * 60 * 1000
    })
  ])
}

middlewares = middlewares.concat([
  // middlewares for common
  conditional(),
  etag(),
  historyApiFallback(historyOptions),
  favicon(config.join.root('favicon.ico')),
  bodyParser(),
  koaWebpackAssets(config.path.assetsFile),
  views(config.dir.views, {
    map: {
      html: 'ejs'
    }
  })
])

export default function () {
  return compose(middlewares)
}
