const webpack = require('webpack')
const helpers = require('./helpers')
const root = helpers.root

const AssetsPlugin = require('assets-webpack-plugin')

module.exports = {
  context: __dirname,
  entry: {
    app: root('client/app.js'),
    vendor: ['vue', 'vue-router']
  },
  output: {
    path: root('public/static'),
    publicPath: '/static/',
    filename: '[name].[chunkhash:6].js',
    chunkFilename: '[name].chunk.[chunkhash:6].js',
    sourceMapFilename: '[name].[chunkhash:6].map'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            scss: 'style-loader!css-loader?importLoaders=1!postcss-loader!sass-loader'
          }
        }
      },
      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true
            }
          },
          'standard-loader'
        ],
        // Try to avoid exclude and prefer include
        include: [root('client')]
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
      {
        test: /\.s[a|c]ss$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
          'postcss-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.styl$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
          'postcss-loader',
          'stylus-loader'
        ]
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
          'postcss-loader'
        ]
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        loader: 'url-loader',
        options: {
          limit: 8192,
          name: 'images/[name].[hash:6].[ext]'
        }
      }, {
        test: /\.(woff|woff2|svg|eot|ttf)\??.*$/,
        loader: 'url-loader',
        options: {
          limit: 8192,
          name: 'fonts/[name].[hash:6].[ext]'
        }
      }
    ]
    // Mark: use 'noParse' to skip parse modules
  },
  resolve: {
    // options for resolving module requests
    // (does not apply to resolving to loaders)

    // extensions that are used
    extensions: ['.js', '.json', '.vue'],

    alias: {
      // a list of module name aliases

      'src': root('client'),
      'components': root('client/components')
    },
    // directories where to look for modules
    modules: [
      root('node_modules'),
      root('client')
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
      }
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new AssetsPlugin({
      path: root('public/static'),
      filename: 'assets.json',
      prettyPrint: true
    })
  ]
}
