const webpack = require('webpack')
const merge = require('webpack-merge')
const base = require('./webpack.base.conf')

base.entry.app = ['webpack-hot-middleware/client', base.entry.app]

module.exports = merge.smart(base, {
  devtool: 'cheap-module-source-map',
  performance: {
    hints: false
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      debug: true
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'vendor.[hash:6].js',
      minChunks: Infinity
    }),
    new webpack.HotModuleReplacementPlugin()
  ]
})

