const webpack = require('webpack')
const merge = require('webpack-merge')
const base = require('./webpack.base.conf')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const WebpackMd5Hash = require('webpack-md5-hash')
const helpers = require('./helpers')
const root = helpers.root

function cssLoader (extraLoader) {
  let cssLoaderBase = [
    {
      loader: 'css-loader',
      options: {
        modules: true,
        minimize: true
      }
    },
    {
      loader: 'postcss-loader'
    }
  ]
  return extraLoader ? cssLoaderBase.concat([{
    loader: extraLoader
  }]) : cssLoaderBase
}

module.exports = merge.smart(base, {
  devtool: 'hidden-source-map',
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            scss: ExtractTextPlugin.extract({
              loader: cssLoader('sass-loader')
            })
          }
        }
      },
      {
        test: /\.js$/,
        use: [
          'babel-loader',
          {
            loader: 'strip-loader',
            options: {
              strip: ['debug', 'console.log']
            }
          }
        ],
        // Try to avoid exclude and prefer include
        include: [root('client')]
      },
      {
        test: /\.s[a|c]ss$/,
        loader: ExtractTextPlugin.extract({
          loader: cssLoader('sass-loader')
        })
      },
      {
        test: /\.styl$/,
        loader: ExtractTextPlugin.extract({
          loader: cssLoader('stylus-loader')
        })
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({
          loader: cssLoader()
        })
      }
    ]
  },
  resolve: {
    // tree-shaking
    mainFields: ['jsnext:main', 'main']
  },
  plugins: [
    // Plugin to replace a standard webpack chunkhash with md5.
    new WebpackMd5Hash(),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'vendor.[chunkhash:6].js',
      minChunks: Infinity
    }),
    // new webpack.optimize.CommonsChunkPlugin({
    //   name: 'commons',
    //   filename: 'commons.bundle.js',
    //   minChunks: 2,
    //   chunks: Object.keys(base.entry).filter(key => key !== 'vendor')
    // }),
    new ExtractTextPlugin({
      filename: '[name].[contenthash:6].css',
      disable: false,
      allChunks: true
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    }),
    // Minimize all JavaScript output of chunks.
    new webpack.optimize.UglifyJsPlugin({
      // Compact the code
      beautify: false,
      // Remove all comments
      comments: false,
      compress: {
        // No warning is output when deleting unused code
        warnings: false,
        // Delete all console code, and compatible with ie browser.
        drop_console: true,
        // Inline variables that are defined but are used only once.
        collapse_vars: true,
        // Extracts static values that occur multiple times but are not dereferenced as variables.
        reduce_vars: true
      }
    })
  ]
})
