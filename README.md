# Koa2-Vue2-Webpack2

## Dependencies

- koa2
- vue2, vue-router
- bulma
- webpack2, babel
- git, git-extras

## Style guide

- [standard](https://github.com/feross/standard)
- [JavaScript Style Guide](https://github.com/airbnb/javascript)
- [clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript)
